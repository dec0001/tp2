using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class declencheur : MonoBehaviour
{
    public GameObject targetObject;  // The object that triggers the respawn
    public Transform spawnPoint;     // The point where the object will respawn
    public Text counterText;         // UI Text element to display the counter

    private int counter = 0;         // Counter to keep track of how many times an object has collided

    // This method is called when another object enters the trigger collider
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == targetObject)
        {
            counter++;  // Increase the counter
            UpdateCounterText();  // Update the UI counter

            // Respawn the target object at the spawn point
            RespawnTarget(other);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Initialize the counter text at the start
        UpdateCounterText();
    }

    // Method to update the counter text
    void UpdateCounterText()
    {
        counterText.text = "Counter: " + counter.ToString();
    }

    // Method to respawn the target object at the spawn point
    void RespawnTarget(Collider other)
    {
        other.transform.position = spawnPoint.position;
        other.transform.rotation = spawnPoint.rotation;

        // Reset velocity if Rigidbody exists (prevents object from carrying over velocity after respawn)
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
